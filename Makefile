

go:
	go run cmd/main.go

swag-init:
	swag init -g api/api.go -o api/docs

migration-up:
	migrate -path ./migrations/postgres/ -database 'postgres://postgres:admin123@localhost:5432/postgres?sslmode=disable' up

migration-down:
	migrate -path ./migrations/postgres/ -database 'postgres://postgres:admin123@localhost:5432/postgres?sslmode=disable' down
