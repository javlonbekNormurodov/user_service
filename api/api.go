package api

import (
	_ "crud/api/docs"
	"crud/api/handler"
	"crud/config"
	"crud/storage"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func SetUpApi(cfg *config.Config, r *gin.Engine, storage storage.StorageI) {

	handlerV1 := handler.NewHandlerV1(cfg, storage)

	r.POST("/person", handlerV1.CreatePerson)
	r.GET("/person/:id", handlerV1.GetPersonById)
	r.GET("/person", handlerV1.GetPeopleList)
	r.PUT("/person/:id", handlerV1.UpdatePerson)
	r.DELETE("/person/:id", handlerV1.DeletePerson)


	url := ginSwagger.URL("swagger/doc.json") // The url pointing to API definition
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
}
