package handler

import (
	"context"
	"errors"
	"log"
	"net/http"
	"strconv"

	"crud/models"

	"github.com/gin-gonic/gin"
)

// CreatePerson godoc
// @ID create_person
// @Router /person [POST]
// @Summary Create Person
// @Description Create Person
// @Tags Person
// @Accept json
// @Produce json
// @Param person body models.CreatePerson true "CreatePersonRequestBody"
// @Success 201 {object} models.Person "GetPersonBody"
// @Response 400 {object} string "Invalid Argument"
// @Failure 500 {object} string "Server Error"
func (h *HandlerV1) CreatePerson(c *gin.Context) {
	var person models.CreatePerson

	err := c.ShouldBindJSON(&person)
	if err != nil {
		log.Printf("error whiling create: %v\n", err)
		c.JSON(http.StatusBadRequest, err.Error())
		return
	}

	id, err := h.storage.Person().Create(context.Background(), &person)
	if err != nil {
		log.Printf("error while Creating: %v\n", err)
		c.JSON(http.StatusInternalServerError, errors.New("error while Creating").Error())
		return
	}

	resp, err := h.storage.Person().GetByPKey(
		context.Background(),
		&models.PersonPrimarKey{Id: id},
	)

	if err != nil {
		log.Printf("error whiling GetByPKey: %v\n", err)
		c.JSON(http.StatusInternalServerError, errors.New("error whiling GetByPKey").Error())
		return
	}

	c.JSON(http.StatusCreated, resp)
}

// GetByIdPerson godoc
// @ID get_by_id_person
// @Router /person/{id} [GET]
// @Summary Get By Id Person
// @Description Get By Id Person
// @Tags Person
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} models.Person "GetPersonBody"
// @Response 400 {object} string "Invalid Argument"
// @Failure 500 {object} string "Server Error"
func (h *HandlerV1) GetPersonById(c *gin.Context) {

	id := c.Param("id")

	resp, err := h.storage.Person().GetByPKey(
		context.Background(),
		&models.PersonPrimarKey{Id: id},
	)

	if err != nil {
		log.Printf("error while GetByPKey: %v\n", err)
		c.JSON(http.StatusInternalServerError, errors.New("error while GetByPKey").Error())
		return
	}

	c.JSON(http.StatusOK, resp)
}

// GetListPeople godoc
// @ID get_list_people
// @Router /person [GET]
// @Summary Get List People
// @Description Get List People
// @Tags Person
// @Accept json
// @Produce json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Success 200 {object} models.GetListPeopleResponse "GetPeopleBody"
// @Response 400 {object} string "Invalid Argument"
// @Failure 500 {object} string "Server Error"
func (h *HandlerV1) GetPeopleList(c *gin.Context) {
	var (
		limit  int
		offset int
		err    error
	)

	limitStr := c.Query("limit")
	if limitStr != "" {
		limit, err = strconv.Atoi(limitStr)
		if err != nil {
			log.Printf("error limit: %v\n", err)
			c.JSON(http.StatusBadRequest, err.Error())
			return
		}
	}

	offsetStr := c.Query("offset")
	if offsetStr != "" {
		offset, err = strconv.Atoi(offsetStr)
		if err != nil {
			log.Printf("error offset: %v\n", err)
			c.JSON(http.StatusBadRequest, err.Error())
			return
		}
	}

	resp, err := h.storage.Person().GetList(
		context.Background(),
		&models.GetListPeopleRequest{
			Limit:  int32(limit),
			Offset: int32(offset),
		},
	)

	if err != nil {
		log.Printf("error while getting list: %v", err)
		c.JSON(http.StatusInternalServerError, errors.New("error while getting list").Error())
		return
	}

	c.JSON(http.StatusOK, resp)

}

// UpdatePerson godoc
// @ID update_person
// @Router /person/{id} [PUT]
// @Summary Update Person
// @Description Update Person
// @Tags Person
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param person body models.UpdatePersonSwagger true "CreatePersonRequestBody"
// @Success 200 {object} models.Person "GetPeopleBody"
// @Response 400 {object} string "Invalid Argument"
// @Failure 500 {object} string "Server Error"
func (h *HandlerV1) UpdatePerson(c *gin.Context) {

	var (
		person models.UpdatePerson
	)

	id := c.Param("id")

	if id == "" {
		log.Printf("error while updating: %v\n", errors.New("required person id").Error())
		c.JSON(http.StatusBadRequest, errors.New("required person id").Error())
		return
	}

	err := c.ShouldBindJSON(&person)
	if err != nil {
		log.Printf("error while updating: %v\n", err)
		c.JSON(http.StatusBadRequest, err.Error())
		return
	}

	person.Id = id

	rowsAffected, err := h.storage.Person().Update(
		context.Background(),
		&person,
	)

	if err != nil {
		log.Printf("error while updating: %v", err)
		c.JSON(http.StatusInternalServerError, errors.New("error while updating").Error())
		return
	}

	if rowsAffected == 0 {
		log.Printf("error while updating rows affected: %v", err)
		c.JSON(http.StatusInternalServerError, errors.New("error while updating rows affected").Error())
		return
	}

	resp, err := h.storage.Person().GetByPKey(
		context.Background(),
		&models.PersonPrimarKey{Id: id},
	)

	if err != nil {
		log.Printf("error while GetByPKey: %v\n", err)
		c.JSON(http.StatusInternalServerError, errors.New("error while GetByPKey").Error())
		return
	}

	c.JSON(http.StatusOK, resp)
}

// DeleteByIdPerson godoc
// @ID delete_by_id_person
// @Router /person/{id} [DELETE]
// @Summary Delete By Id Person
// @Description Delete By Id Person
// @Tags Person
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} models.Person "GetPersonBody"
// @Response 400 {object} string "Invalid Argument"
// @Failure 500 {object} string "Server Error"
func (h *HandlerV1) DeletePerson(c *gin.Context) {

	id := c.Param("id")
	if id == "" {
		log.Printf("error while deleting: %v\n", errors.New("required person id").Error())
		c.JSON(http.StatusBadRequest, errors.New("required person id").Error())
		return
	}

	err := h.storage.Person().Delete(
		context.Background(),
		&models.PersonPrimarKey{
			Id: id,
		},
	)

	if err != nil {
		log.Printf("error whiling delete: %v", err)
		c.JSON(http.StatusInternalServerError, errors.New("error while deleting").Error())
		return
	}

	c.JSON(http.StatusNoContent, nil)
}
