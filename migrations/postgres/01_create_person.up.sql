CREATE TABLE person (
    id UUID PRIMARY KEY NOT NULL,
    name VARCHAR NOT NULL,
    last_name VARCHAR NOT NULL DEFAULT 0,
    middle_name VARCHAR NOT NULL DEFAULT '',
    age INT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    is_deleted BOOLEAN DEFAULT false,
    deleted_at TIMESTAMP
);
