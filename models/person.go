package models

type PersonPrimarKey struct {
	Id string `json:"id"`
}

type CreatePerson struct {
	Name       string `json:"name"`
	LastName   string `json:"last_name"`
	MiddleName string `json:"middle_name"`
	Age        int    `json:"age"`
}

type Person struct {
	Id         string `json:"id"`
	Name       string `json:"name"`
	LastName   string `json:"last_name"`
	MiddleName string `json:"middle_name"`
	Age        int    `json:"age"`
	CreatedAt  string `json:"created_at"`
	UpdatedAt  string `json:"updated_at"`
	DeletedAt  string `json:"deleted_at"`
}

type UpdatePersonSwagger struct {
	Name       string `json:"name"`
	LastName   string `json:"last_name"`
	MiddleName string `json:"middle_name"`
	Age        string `json:"age"`
}

type UpdatePerson struct {
	Id         string `json:"id"`
	Name       string `json:"name"`
	LastName   string `json:"last_name"`
	MiddleName string `json:"middle_name"`
	Age        string `json:"age"`
}

type GetListPeopleRequest struct {
	Limit  int32
	Offset int32
}

type GetListPeopleResponse struct {
	Count  int      `json:"count"`
	People []Person `json:"people"`
}
