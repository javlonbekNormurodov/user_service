package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"crud/models"
	"crud/pkg/helper"
)

type PersonRepo struct {
	db *pgxpool.Pool
}

func NewPersonRepo(db *pgxpool.Pool) *PersonRepo {
	return &PersonRepo{
		db: db,
	}
}

func (f *PersonRepo) Create(ctx context.Context, person *models.CreatePerson) (string, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO person(
			id,
			name,
			last_name,
			middle_name,
			age,
			updated_at
		) VALUES ( $1, $2, $3, $4, $5, now() )
	`

	_, err := f.db.Exec(ctx, query,
		id,
		person.Name,
		person.LastName,
		person.MiddleName,
		person.Age,
	)

	if err != nil {
		return "", err
	}

	return id, nil
}

func (f *PersonRepo) GetByPKey(ctx context.Context, pkey *models.PersonPrimarKey) (*models.Person, error) {

	var (
		id          sql.NullString
		name        sql.NullString
		last_name   sql.NullString
		middle_name sql.NullString
		age         sql.NullInt32
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	query := `
		SELECT
			id,
			name,
			last_name,
			middle_name,
			age,
			created_at,
			updated_at
		FROM
			person
		WHERE id = $1
	`

	err := f.db.QueryRow(ctx, query, pkey.Id).
		Scan(
			&id,
			&name,
			&last_name,
			&middle_name,
			&age,
			&createdAt,
			&updatedAt,
		)

	if err != nil {
		return nil, err
	}

	return &models.Person{
		Id:         id.String,
		Name:       name.String,
		LastName:   last_name.String,
		MiddleName: middle_name.String,
		Age:        int(age.Int32),
		CreatedAt:  createdAt.String,
		UpdatedAt:  updatedAt.String,
	}, nil
}

func (f *PersonRepo) GetList(ctx context.Context, req *models.GetListPeopleRequest) (*models.GetListPeopleResponse, error) {

	var (
		resp   = models.GetListPeopleResponse{}
		offset = ""
		limit  = ""
	)

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	query := `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			last_name,
			middle_name,
			age,
			created_at,
			updated_at
		FROM
			person
	`

	query += offset + limit

	rows, err := f.db.Query(ctx, query)

	for rows.Next() {

		var (
			id          sql.NullString
			name        sql.NullString
			last_name   sql.NullString
			middle_name sql.NullString
			age         sql.NullInt32
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&last_name,
			&middle_name,
			&age,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return nil, err
		}

		resp.People = append(resp.People, models.Person{
			Id:         id.String,
			Name:       name.String,
			LastName:   last_name.String,
			MiddleName: middle_name.String,
			Age:        int(age.Int32),
			CreatedAt:  createdAt.String,
			UpdatedAt:  updatedAt.String,
		})

	}

	return &resp, err
}

func (f *PersonRepo) Update(ctx context.Context, req *models.UpdatePerson) (int64, error) {

	var (
		query  = ""
		params map[string]interface{}
	)

	query = `
		UPDATE
			person
		SET
			name = :name,
			last_name = :last_name,
			middle_name = :middle_name,
			age = :age,
			updated_at = now()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":          req.Id,
		"name":        req.Name,
		"last_name":   req.LastName,
		"middle_name": req.MiddleName,
		"age":         req.Age,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	rowsAffected, err := f.db.Exec(ctx, query, args...)
	if err != nil {
		return 0, err
	}

	return rowsAffected.RowsAffected(), nil
}

func (f *PersonRepo) Delete(ctx context.Context, req *models.PersonPrimarKey) error {

	_, err := f.db.Exec(ctx, "UPDATE person SET deleted_at = now(), is_deleted = true WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return err
}
