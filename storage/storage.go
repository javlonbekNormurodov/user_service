package storage

import (
	"context"

	"crud/models"
)

type StorageI interface {
	CloseDB()
	Person() PersonRepoI
}

type PersonRepoI interface {
	Create(ctx context.Context, req *models.CreatePerson) (string, error)
	GetByPKey(ctx context.Context, req *models.PersonPrimarKey) (*models.Person, error)
	GetList(ctx context.Context, req *models.GetListPeopleRequest) (*models.GetListPeopleResponse, error)
	Update(ctx context.Context, req *models.UpdatePerson) (int64, error)
	Delete(ctx context.Context, req *models.PersonPrimarKey) error
}
